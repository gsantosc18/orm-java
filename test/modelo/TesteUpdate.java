/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import modelo.sql.ConnectionFactory;
import modelo.sql.Update;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author myhouse
 */
public class TesteUpdate {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection connection = ConnectionFactory.getConnection();
        
        Update update = new Update(connection);
        
        update
            .table("instituicao")
            .set(
                    new Object[]{"nome","url"},
                    new Object[]{"Gedalias","www"}
            )
            .condition(
                new Object[]{"id_instituicao"},
                new Object[]{10}
            )
            .execute();
    }
}
