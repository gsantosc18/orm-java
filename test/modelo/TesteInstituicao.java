/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.bean.Instituicao;
import modelo.dao.InstituicaoDAO;
import modelo.sql.ConnectionFactory;
/**
 *
 * @author myhouse
 */
public class TesteInstituicao {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection connection = ConnectionFactory.getConnection();
        
        Instituicao instituicao = new Instituicao("Aida", "aida");
        
        InstituicaoDAO instituicaoDAO = new InstituicaoDAO(connection);
        
        ResultSet rs = instituicaoDAO.all();
        
        while(rs.next()){
            System.out.println(rs.getString("nome"));
        }
    }
}
