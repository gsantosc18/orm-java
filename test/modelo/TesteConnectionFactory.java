/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import modelo.sql.ConnectionFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author myhouse
 */
public class TesteConnectionFactory {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        
        Connection connection = ConnectionFactory.getConnection();        
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("select * from smtp");
        
        while(rs.next()){
            System.out.println(rs.getString("dominio"));
        }
    }
}
