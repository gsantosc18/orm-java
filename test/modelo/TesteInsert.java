/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import modelo.sql.ConnectionFactory;
import modelo.sql.Insert;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author myhouse
 */
public class TesteInsert {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection connection = ConnectionFactory.getConnection();
        
        Insert insert = new Insert(connection);
        
        insert.table("instituicao").columns("nome, url");
        insert.addValues(new Object[][]{{"Gedalias","www.google.com"},{"Gedalias","www.google.com"}});
        
        insert.execute();
    }
}
