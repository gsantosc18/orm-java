/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import modelo.sql.Select;
import modelo.sql.ConnectionFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author myhouse
 */
public class TesteSelect {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection connection = ConnectionFactory.getConnection();
        
        Select select = new Select(connection);
        ResultSet rs = select
            .table("smtp")
            .selection("dominio")
            .condition(
                new Object[]{"id_smtp"},
                new Object[]{10}
            )
            .execute();
        
        while(rs.next()){
            System.out.println(rs.getString("dominio"));
        }
    }
}
