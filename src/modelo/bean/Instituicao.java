/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.bean;

/**
 *
 * @author myhouse
 */
public class Instituicao {
    private int id_instituicao;
    private String nome;
    private String url;
    
    public Instituicao(String nome, String url){
        this.nome = nome;
        this.url = url;
    }

    /**
     * @return the id_instituicao
     */
    public int getId_instituicao() {
        return id_instituicao;
    }

    /**
     * @param id_instituicao the id_instituicao to set
     */
    public void setId_instituicao(int id_instituicao) {
        this.id_instituicao = id_instituicao;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
