/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import modelo.bean.Instituicao;
import modelo.sql.Insert;
import modelo.sql.Select;

/**
 *
 * @author myhouse
 */
public class InstituicaoDAO {
    protected String table = "instituicao";
    protected Connection connection;
    
    public InstituicaoDAO(Connection connection){
        this.connection = connection;
    }
    
    public ResultSet all(){
        return new Select(connection)
                .table(this.table)
                .execute();
    }
    
    public Integer add(Instituicao instituicao){
        return new Insert(connection)
            .table(this.table)
                .columns("nome,url")
            .addValues(
                new Object[][]{
                    {instituicao.getNome(),instituicao.getUrl()}
                }
            )
            .execute();
    }
}
