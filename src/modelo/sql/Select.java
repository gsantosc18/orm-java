/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Select implements SQLInterface{

    protected Connection connection;
    protected String table;
    protected String condition;
    protected String selection;
    
    /**
     * Construtor da classe Select
     * @param connection 
     */
    public Select(Connection connection){
        this.connection = connection;
        this.selection = "*";
        this.condition = "";
    }

    /**
     * Especifica a tabela a ser consultada
     * @param table
     * @return Select
     */
    public Select table(String table){
        this.table = table;
        return this;
    }
    
    /**
     * Criar a projeto dos campos da tabela
     * @param selection
     * @return 
     */
    public Select selection(String selection){
        this.selection = selection;
        return this;
    }
    
    /**
     * Criar as condicoes para a consulta
     * @param columns
     * @param values
     * @return 
     */
    public Select condition(Object[] columns, Object[] values){
        for (int i = 0; i < columns.length; i++) {
            this.condition += String.format("%s='%s'", columns[i],values[i]);
            if(i!=columns.length-1) this.condition+=",";
        }
        return this;
    }
    
    @Override
    public ResultSet execute() {        
        
        String sql = String.format("SELECT %s FROM %s %s", getSelection(), getTable(), getCondition());
        
        try {
            return connection.createStatement().executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Select.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Retorna a condicao do SQL
     * @return 
     */
    private String getCondition(){        
        if(!this.condition.trim().isEmpty()){
            return String.format("WHERE %s", this.condition);
        }        
        return this.condition;
    }
    
    /**
     * Retorna a projecao da tabela a ser consultada
     * @return 
     */
    private String getSelection(){
        return this.selection;
    }
    
    /**
     * Retorna a tabela a ser consultada
     * @return 
     */
    private String getTable(){
        return this.table;
    }
    
}
