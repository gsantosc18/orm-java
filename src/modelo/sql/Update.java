/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author myhouse
 */
public class Update implements SQLInterface{

    protected Connection connection;
    protected String table;
    protected String seters;
    protected String conditions;
    protected List<Object> parameters;
    
    public Update(Connection connection){
        this.connection = connection;
        this.table = "";
        this.seters = "";
        this.conditions = "";
        this.parameters = new ArrayList<>();
    }

    @Override
    public Update table(String table) {
        this.table = table;
        return this;
    }
    
    public Update set(Object[] columns,Object[] values){
        for (int i = 0; i < columns.length; i++) {
            this.seters += String.format("%s=?", columns[i]);
            this.parameters.add(values[i]);
            if(i!=columns.length-1) this.seters+=",";
        }
        return this;
    }
    
    public Update condition(Object[] columns, Object[] values){
        for (int i = 0; i < columns.length; i++) {
            this.conditions += String.format("%s=?", columns[i]);
            this.parameters.add(values[i]);
            if(i!=columns.length-1) this.conditions+=",";
        }
        return this;
    }
    
    
    @Override
    public Integer execute() {
        String sql = String.format("UPDATE %s SET %s %s", getTable(), getSeters(), getConditions());
        
        try {
            PreparedStatement preparedStmt = connection.prepareStatement(sql);
            
            for (int i = 0; i < parameters.size(); i++){
                preparedStmt.setObject(i+1, parameters.get(i));                
            }
            
            return preparedStmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Select.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * @return the table
     */
    private String getTable() {
        return table;
    }

    /**
     * @return the seters
     */
    private String getSeters() {
        return seters;
    }

    /**
     * @return the conditions
     */
    private String getConditions() {
        if(!conditions.isEmpty()){
            return "WHERE "+conditions;
        }
        return conditions;
    }
}
