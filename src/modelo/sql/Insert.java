/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author myhouse
 */
public class Insert implements SQLInterface{
    protected Connection connection;
    private String table;
    private String columns;
    private String values;
    
    /**
     * Construtor da classe
     * @param connection 
     */
    public Insert(Connection connection){
        this.connection = connection;
        this.values = "";
    }
    
    /**
     * Informa a tabela que será usada
     * @param table
     * @return 
     */
    public Insert table(String table){
        this.table = table;
        return this;
    }
    
    /**
     * Informa as colunas que serao inseridas na tabela
     * @param columns
     * @return 
     */
    public Insert columns(String columns){
        this.columns = columns;
        return this;
    }
    
    public Insert addValues(Object[][] values){
        
        for (Object[] columns : values) {
            String insertValues = "";
            
            for (Object value : columns) {
                insertValues += String.format("'%s',", value);
            }
            
            if(!this.values.trim().isEmpty()){
                this.values +=",";
            }
            
            this.values += String.format("(%s)",insertValues.substring(0,insertValues.length()-1));
        }
        
        
        
        return this;
    }

    @Override
    public Integer execute() {
        String sql = String.format("INSERT INTO '%s'(%s) VALUES %s", getTable(), getColumns(), getValues());
        
        System.out.println(sql);
        
        try {
            return connection.createStatement().executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Select.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    /**
     * @return the table
     */
    private String getTable() {
        return table;
    }

    /**
     * @return the columns
     */
    private String getColumns() {
        return columns;
    }

    /**
     * @return the values
     */
    private String getValues() {
        return values;
    }
}
