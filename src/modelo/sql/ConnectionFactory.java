/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author myhouse
 */
public class ConnectionFactory {
    private static final String DRIVER = "org.sqlite.jdbc4.JDBC4Connection";
    private static final String URL = "jdbc:sqlite:smtp.db";
    private static final String USERNAME = "";
    private static final String PASSWORD = "";
    
    private static Connection connect = null; 
    
    private ConnectionFactory(){}
    
    public static Connection getConnection() throws ClassNotFoundException, SQLException{
        if(connect == null){
            Class.forName(DRIVER);
            connect = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        }
        return connect;
    }
}
