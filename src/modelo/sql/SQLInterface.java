/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sql;

/**
 *
 * @author myhouse
 */
public interface SQLInterface {
    
    /**
     * Especifica a tabela a ser consultada
     * @param table
     * @return 
     */
    public SQLInterface table(String table);
    
    /**
     * Executa a instrucao formada pelo metodos
     * @return 
     */
    public Object execute();
}
